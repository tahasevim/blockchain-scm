// Compiler version:0.5.12+commit.7709ece9.Emscripten.clang
pragma solidity 0.5.12;

import "./PC.sol";

// Second producer-customer contract.
// Inherits common attributes and functions from abstract PC contract.
contract PC2 is PC {
    
    // Flour
    struct Flour {
        uint wheatID; // Wheat ID to indicate which wheat is used
        uint id; // Flour id
        string flourType; // Flour type
        string brand; // Flour brand
        uint totalWeight; // Total weight
    }
    
    // Proposal
    struct Proposal {
        Flour flour; // Proposed Flour
        string arrivalTime; // Proposed arrival time
        uint proposedPrice; // Proposed price
    }
    
    // ProducedProduct
    struct ProducedProduct {
        Flour flour; // Produced Flour
        string estimatedArrivalTime; // Estimation time for produced Flour
    }
    
    // History
    struct History {
        Flour flour; // Past Flour
        uint price; // Price for the past Flour
        string arrivalTime; // Arrival time for the past Flour
    }
    
    Proposal proposal; // Current proposal
    ProducedProduct producedProduct; // Current produced product
    mapping(uint => History) flourHistory; // Historical collection for Flour

    
    function() external payable { revert(); } 

    constructor (address payable producerAddr, address payable customerAddr) PC(producerAddr, customerAddr)  public {}
    
    // Customer proposes his desired flour specification and sends price to the contract.
    function Propose(string memory flourType, string memory brand, uint totalWeight, string memory arrivalTime) public payable onlyCustomer(msg.sender){
        proposal = Proposal({
            flour: Flour({
               wheatID: 0,
               id:0,
               flourType: flourType,
               brand: brand,
               totalWeight: totalWeight
            }),
            arrivalTime: arrivalTime,
            proposedPrice: msg.value
        });
        customer.productProposalMoney = msg.value;
        contractBalance += msg.value;
    }
    
    // Producer sends produced flour specifications to the contract.
    function SetProducedProduct(uint wheatID, uint id, string memory flourType, string memory brand, uint totalWeight, string memory estimatedArrivalTime) public onlyProducer(msg.sender){
        producedProduct = ProducedProduct({
            flour: Flour({
               wheatID: wheatID,
               id: id,
               flourType: flourType,
               brand: brand,
               totalWeight: totalWeight
            }),
            estimatedArrivalTime: estimatedArrivalTime
        });
    }
    
    // Owner resets agreement to be ready for next agreements.
    function ResetAgreement() external onlyOwner(msg.sender){
        if(customer.acceptProduct){
            flourHistory[producedProduct.flour.id] = History({
                flour: producedProduct.flour,
                price: customer.productProposalMoney,
                arrivalTime: producedProduct.estimatedArrivalTime
            });
            delete proposal;
            delete producedProduct;
            
            producer.acceptProposal = false;
            customer.acceptProduct = false;
            customer.hasMadeProposal = false;
            customer.productProposalMoney = 0;
        }
    }
    
    // Returns flour specifications from historical data collection.
    function GetProduct(uint id) external view returns (uint, string memory, string memory, uint, string memory, uint){
        History memory history = flourHistory[id];
        return (history.flour.wheatID, history.flour.flourType, history.flour.brand, history.flour.totalWeight, history.arrivalTime, history.price);
    }
}
