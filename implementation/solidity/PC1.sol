// Compiler version:0.5.12+commit.7709ece9.Emscripten.clang
pragma solidity 0.5.12;

import "./PC.sol";

// First producer-customer contract.
// Inherits common attributes and functions from abstract PC contract.
contract PC1 is PC {
    
    // Wheat
    struct Wheat {
        uint id; // Wheat id
        string wheatType; // Wheat type
        string origin; // Wheat origin
        string harvestTime; // Weat harvest time
    }
    
    // Proposal
    struct Proposal {
        Wheat wheat; // Proposed Wheat
        string arrivalTime; // Proposed arrival time
        uint proposedPrice; // Proposed price
    }
    
    // ProducedProduct
    struct ProducedProduct {
        Wheat wheat; // Produced Wheat
        string estimatedArrivalTime; // Estimation time for produced Wheat
    }
    
    // History
    struct History {
        Wheat wheat; // Past Wheat
        uint price; // Price for past Wheat
        string arrivalTime; // Arrival time for past Wheat
    }
    
    Proposal proposal; // Current proposal
    ProducedProduct producedProduct; // Current produced product
    mapping(uint => History) wheatHistory; // Historical collection for Wheat

    function() external payable { revert(); } 

    constructor (address payable producerAddr, address payable customerAddr) PC(producerAddr, customerAddr)  public {}

    // Customer proposes his desired wheat specification and sends price to the contract.
    function Propose(string memory wheatType, string memory origin, string memory harvestTime, string memory arrivalTime) public payable onlyCustomer(msg.sender) returns (uint){
        proposal = Proposal({
            wheat: Wheat({
               id: 0,
               wheatType: wheatType,
               origin: origin,
               harvestTime: harvestTime
            }),
            arrivalTime: arrivalTime,
            proposedPrice: msg.value
        });
        customer.hasMadeProposal = true;
        customer.productProposalMoney = msg.value;
        contractBalance += msg.value;
        return contractBalance;
    }
    
    // Producer sends produced wheat specifications to the contract.
    function SetProducedProduct(uint id, string memory wheatType, string memory origin, string memory harvestTime,  string memory estimatedArrivalTime) public onlyProducer(msg.sender){
        producedProduct = ProducedProduct({
            wheat: Wheat({
               id: id, 
               wheatType: wheatType,
               origin: origin,
               harvestTime: harvestTime
            }),
            estimatedArrivalTime: estimatedArrivalTime
        });
    }
    
    // Owner resets agreement to be ready for next agreements.
    function ResetAgreement() external onlyOwner(msg.sender) returns (bool){
        if(customer.acceptProduct){
            wheatHistory[producedProduct.wheat.id] = History({
                wheat: producedProduct.wheat,
                price: customer.productProposalMoney,
                arrivalTime: producedProduct.estimatedArrivalTime
            });
            delete proposal;
            delete producedProduct;
            producer.acceptProposal = false;
            customer.acceptProduct = false;
            customer.hasMadeProposal = false;
            customer.productProposalMoney = 0;
            return true;
        }
    }
    
    // Returns wheat specifications from historical data collection.
    function GetProduct(uint id) external view returns (string memory, string memory, string memory, string memory, uint){
        History memory history = wheatHistory[id];
        return (history.wheat.wheatType, history.wheat.origin, history.wheat.harvestTime, history.arrivalTime, history.price);
    }
}
