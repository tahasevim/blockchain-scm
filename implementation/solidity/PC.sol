// Compiler version:0.5.12+commit.7709ece9.Emscripten.clang
pragma solidity 0.5.12;

// Abstract contract for commons in producer-customer contracts
contract PC {
    
    // Producer
    struct Producer {
        address payable addr; // Producer address
        bool acceptProposal; // Flag to indicate whether producer accepted proposal or not
    }
    
    // Customer
    struct Customer {
        address payable addr; // Customer address
        bool acceptProduct; // Flag to indicate whether customer accepted product or not
        bool hasMadeProposal; // Flag to indicate whether customer made proposal or not
        uint productProposalMoney; // Offer for the product
    }
    
    address owner; // Owner of the contract
    uint contractBalance; // Contract Balance
    
    Producer producer; // Producer of the contract
    Customer customer; // Customer of the contract
    
    modifier onlyOwner(address addr) { if (addr == owner) _; } // Modifier to restrict functions to be used only by Owner
    modifier onlyCustomer(address addr) { if (addr == customer.addr) _; } // Modifier to restrict functions to be used only by Customer
    modifier onlyProducer(address addr) { if (addr == producer.addr) _; } // Modifier to restrict functions to be used only by Producer
    modifier isProductDenied() { if (producer.acceptProposal && !customer.acceptProduct) _; } // Modifier to restrict functions to be used only when product is denied
    
    function() external payable { revert(); } 

    constructor (address payable producerAddr, address payable customerAddr) internal {
        owner = msg.sender;
        contractBalance = 0;
        producer = Producer({
            addr: producerAddr,
            acceptProposal: false
        });
        customer = Customer({
            addr: customerAddr,
            acceptProduct: false,
            hasMadeProposal: false,
            productProposalMoney: 0
        });
        
    }
    
    // Returns owner of the contract
    function GetOwner() external view returns (address){
        return owner;
    }
    
    // Returns producer of the contract
    function GetProducer() external view returns (address, bool){ 
        return (producer.addr, producer.acceptProposal);
    }
    
    // Returns customer of the contract
    function GetCustomer() external view returns (address, bool, bool, uint){
        return (customer.addr, customer.acceptProduct, customer.hasMadeProposal, customer.productProposalMoney);
    }
    
    // Vote function for producer to accept or deny proposal of the customer.
    function VoteProposal(bool vote) external onlyProducer(msg.sender) returns (bool){
        if (customer.hasMadeProposal && !vote && contractBalance >= customer.productProposalMoney){
            contractBalance -= customer.productProposalMoney;
            if(!customer.addr.send(customer.productProposalMoney))
                contractBalance += customer.productProposalMoney;
        } else 
            producer.acceptProposal = true;
        return producer.acceptProposal;
    }
    
    // Vote function for customer to accept or deny product that producer produced.
    function VoteProduct(bool vote) external onlyCustomer(msg.sender) returns (bool){
        if(producer.acceptProposal && vote && contractBalance >= customer.productProposalMoney){
            contractBalance -= customer.productProposalMoney;
            if(!producer.addr.send(customer.productProposalMoney))
                contractBalance += customer.productProposalMoney;
            else
                customer.acceptProduct = true;
        }
        return customer.acceptProduct;
    }
    
    // Producer votes to specifiy product is sent back or not if product is denied.
    function HasReceivedBack(bool vote) external onlyProducer(msg.sender) isProductDenied() {
        if(vote && contractBalance >= customer.productProposalMoney ){
            contractBalance -= customer.productProposalMoney;
            if(!customer.addr.send(customer.productProposalMoney))
                contractBalance += customer.productProposalMoney; 
         
        }
    }

    
}
