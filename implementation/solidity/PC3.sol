// Compiler version:0.5.12+commit.7709ece9.Emscripten.clang
pragma solidity 0.5.12;

import "./PC.sol";

// Second producer-customer contract.
// Inherits common attributes and functions from abstract PC contract.
contract PC3 is PC {
    
    // Bread
    struct Bread {
        uint flourID; // Flour ID to indicate which flour is used
        uint id; // Bread id
        string breadType; // Bread type
        uint water; // Water percentage in bread
        uint salt; // Salt percentage in bread
        string yeastType; // Yeast type for bread
        uint totalWeight; // Total weight of bred
    }
    
    // Proposal
    struct Proposal {
        Bread bread; // Proposed Bread
        string arrivalTime; // Proposed arrival time
        uint proposedPrice; // Proposed price
    }
    
    // ProducedProuct
    struct ProducedProduct {
        Bread bread; // Produced Bread
        string estimatedArrivalTime; // Estimation time for produced Flour
    }
    
    // History
    struct History {
        Bread bread; // Past Bread
        uint price; // Price for the past Bread
        string arrivalTime; // Arrival time for the past Bread
    }
    
    Proposal proposal; // Current proposal
    ProducedProduct producedProduct; // Current produced product
    mapping(uint => History) breadHistory; // Historical collection for Bread

    function() external payable { revert(); } 
        
    constructor (address payable producerAddr, address payable customerAddr) PC(producerAddr, customerAddr)  public {}
    
    // Customer proposes his desired bread specification and sends price to the contract.
    function Propose(string memory breadType, uint water, uint salt, string memory yeastType, uint totalWeight, string memory arrivalTime) public payable onlyCustomer(msg.sender) {
        proposal = Proposal({
            bread: Bread({
               flourID: 0,
               id: 0,
               breadType: breadType,
               water: water,
               salt: salt,
               yeastType: yeastType,
               totalWeight: totalWeight
            }),
            arrivalTime: arrivalTime,
            proposedPrice: msg.value
        });
        customer.productProposalMoney = msg.value;
        contractBalance += msg.value;
    }
    
    // Producer sends produced bread specifications to the contract.
    function SetProducedProduct(uint flourID, uint id, string memory breadType, uint water, uint salt, string memory yeastType, uint totalWeight, string memory estimatedArrivalTime) public onlyProducer(msg.sender){
        producedProduct = ProducedProduct({
            bread: Bread({
               flourID: flourID,
               id: id,
               breadType: breadType,
               water: water,
               salt: salt,
               yeastType: yeastType,
               totalWeight: totalWeight
            }),
            estimatedArrivalTime: estimatedArrivalTime
        });
    }
    
    // Owner resets agreement to be ready for next agreements.
    function ResetAgreement() external onlyOwner(msg.sender){
        if(customer.acceptProduct){
            breadHistory[producedProduct.bread.id]= History({
                bread: producedProduct.bread,
                price: customer.productProposalMoney,
                arrivalTime: producedProduct.estimatedArrivalTime
            });
            delete proposal;
            delete producedProduct;
            producer.acceptProposal = false;
            customer.acceptProduct = false;
            customer.hasMadeProposal = false;
            customer.productProposalMoney = 0;
        }
    }
    
    // Returns bread specifications from historical data collection.
    function GetProduct(uint id) external view returns (uint, string memory, uint, uint, string memory, uint, string memory, uint){
        History memory history = breadHistory[id];
        return (history.bread.flourID, history.bread.breadType, history.bread.water, history.bread.salt, history.bread.yeastType, history.bread.totalWeight, history.arrivalTime, history.price);
    }
}
