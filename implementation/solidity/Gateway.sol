// Compiler version:0.5.12+commit.7709ece9.Emscripten.clang
pragma solidity 0.5.12;

import "./PC.sol";
import "./PC1.sol";
import "./PC2.sol";
import "./PC3.sol";

// Gateway contract
contract Gateway {
    
    address owner;
    mapping (uint => PC) contracts; // Holds PC1, PC2, PC3
    uint totalContractNum; // Total # of contracts
    PC1 pc1; // First producer-customer contract
    PC2 pc2; // Second producer-customer contract
    PC3 pc3; // Third producer-customer contract

    function() external payable { revert(); } 

    constructor (address payable pc1Addr, address payable pc2Addr, address payable pc3Addr) public {
        owner = msg.sender;
        pc1 = PC1(pc1Addr);
        pc2 = PC2(pc2Addr);
        pc3 = PC3(pc3Addr);
        
        contracts[1] = pc1;
        contracts[2] = pc2;
        contracts[3] = pc3;
        
        totalContractNum = 3;
    }
    
    // Returnrs total number of contracts (3)
    function GetNumOfContracts () external view returns (uint){
        return totalContractNum;
    }
    
    // Returns owner of the contract according to given contractID
    function GetPCOwner(uint contractID) external view returns (address) {
        return contracts[contractID].GetOwner();
    }
    
    // Returns customer of the contract according to given contractID
    function GetPCCustomer(uint contractID) external view returns (address, bool, bool, uint) {
        return contracts[contractID].GetCustomer();
    }
    
    // Returns producer of the contract according to given contractID
    function GetPCProducer(uint contractID) external view returns (address, bool) {
        return contracts[contractID].GetProducer();
    }
    
    // Returns wheat specification data according to given id 
    function GetPC1Product (uint id) external view returns (string memory, string memory, string memory, string memory, uint) {
        return pc1.GetProduct(id);
    }
    
    // Returns flour specification data according to given id 
    function GetPC2Product (uint id) external view returns (uint, string memory, string memory, uint, string memory, uint) {
        return pc2.GetProduct(id);
    }
    
    // Returns bread specification data according to given id 
    function GetPC3Product (uint id) external view returns (uint, string memory, uint, uint, string memory, uint, string memory, uint) {
        return pc3.GetProduct(id);
    }
}
